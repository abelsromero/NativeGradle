#include "println.h"

#include <iostream>
#include <string>

int main(int argc,char** argv) {

    if(argc < 2) {
        std::cout << "Usage: hello <name>" << std::endl;
        return 1;
    }

    std::string msg("Hello, ");
    msg+= argv[1] ;
    ::println(msg.c_str());
    
    return 0;
}