#include <iostream>
#include <println.hpp>

namespace gradleExamples {

void println( std::ostream& strm, std::string const& text) {
    strm << text << std::endl;
}

void println( std::string const& text) {
    println( std::cout, text );
}

} // namespace gradleExamples
