[[cpp-simple-lib]] Create a new project directory and within that create the following `src/println/headers/println.hpp` and a `src/println/cpp/println.cpp` files

.src/println/headers/println.hpp
[source,cpp]
----
include::{examples}/simple-cpp-lib/src/println/headers/println.hpp[]
----

.src/println/cpp/println.cpp
[source,cpp]
----
include::{examples}/simple-cpp-lib/src/println/cpp/println.cpp[]
----
